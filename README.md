# Technical Interview Cheat Sheet  
  
## Topics  


- Big O's for the following  
    * DFS BFS, Binary search  
    * Merge sort, quick sort, bubble sort, heapsort, bucket sort, insertion sort  
    * Binary tree, B-tree, Red-black tree, AVL tree  
    * heap, array, queue, stack, ;hash table, linkedlist  
    * OS Scheduling, processes, threads  